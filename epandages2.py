from tkinter import *
from decimal import *
from readfile import *
class epandages2:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='Quelle type d\'épandage ?')
        self.texte1['fg']='red'
        self.freq = trt_file(data.getfilename(),Fenetre)
        self.texte2=Label(Fenetre,text='pour combien d\'unité d\'azote efficace(voir annexe 19)?')
        self.texte2['fg']='red'
        self.autres = Checkbutton(Fenetre, text="avez vous fais d'autres type d'apport",command=self.pred)
        self.spin = Spinbox(Fenetre,from_=0,to=1000000)
        self.pred=0
        self.p.add(self.texte1)
        self.p.add(self.freq.getliste())
        self.p.add(self.texte2)
        self.p.add(self.spin)
        self.p.add(self.autres)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def pred(self):
        if (self.pred == 1):
            self.pred =0
        else:
            self.pred =1
    def getphrase(self):
        return "\nvotre type d'épendage est : "+self.freq.getstringliste()+" pour"+str(self.spin.get())+" unité d'azote efficace pour un P0 de:"+str(self.getvalue())
    def getpred(self):
        return self.pred

    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()


    def getvalue(self):
        x =Decimal(self.freq.getvalueliste())
        y=Decimal(0)
        if(self.freq.getindliste()[0]==0):
            y=Decimal(210)
        elif(self.freq.getindliste()[0]==1):
            y=Decimal(250)
        elif(self.freq.getindliste()[0]==2):
            y=Decimal(70)
        else:
            y=Decimal(140)
        print(x)
        print(y)
        print(self.freq.getindliste())
        return Decimal(Decimal(Decimal(x)/Decimal(y))*Decimal(self.spin.get()))
