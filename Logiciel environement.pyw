from tkinter import *
from menu import *
from calcul_nitrate import *
from dep_esp import *
from calcul import *
from decimal import *
from rendement import *
from sol import *
from selectvaluefile import *
from precedent import *
from epandages import *
from epandages2 import *
from prairie import *
from prairie2 import *
from nirr import *
from cultinter import *
from cultinter2 import *
from engraisorg import *
from EndScreen import *
from ble import *
from Ri import *
#premiére event
def event_nitrate (menuprincip,Fenetre,bool):
    nitr_inter=type_culture(Fenetre,event_menu,event_nitrate2,event_menu)
    menuprincip.clear()
    nitr_inter.print()
#deusieme
def event_nitrate2 (ancienne,Fenetre,bool,data):
    if (bool == 1):
        data=calcul()
        data.defineresume1(ancienne.getphrase())
        data.definePF(ancienne.getvalue())
        data.defineTypeProduction(ancienne.getstring())
        data.defineDep(ancienne.getdep())
        data.defineform(ancienne.getform())
        x=ancienne.getble()
    else:
        x=2
    ancienne.clear()
    name='BDD/' + data.getDep() + 'espece_cult.txt'
    if (x == 1):
        ancienne.clear()
        bler=ble(Fenetre,event_menu,event_nitrate2bis,event_nitrate,data)
        bler.print()
    else:
        name='BDD/' + data.getDep() + 'espece_cult.txt'
        if (data.getform() == 0):
            inter_dep_esp=dep_esp(Fenetre,name,event_menu,endPF,event_nitrate,data)
            inter_dep_esp.print()
        else:
            rendement_prec=rendement(Fenetre,event_menu,event_nitrate,endPF,data)
            rendement_prec.print()
#troisiéme

def event_nitrate2bis(ancienne,Fenetre,data,bool):
    if (bool == 1):
        data.defineresume1(ancienne.getphrase())
        data.definePF(ancienne.getvalue())
    ancienne.clear()
    name='BDD/' + data.getDep() + 'espece_cult.txt'
    if (data.getform() == 0):
        inter_dep_esp=dep_esp(Fenetre,name,event_menu,endPF,event_nitrate,data)
        inter_dep_esp.print()
    else:
        rendement_prec=rendement(Fenetre,event_menu,endPF,event_nitrate,data)
        rendement_prec.print()

def endPF (ancienne,Fenetre,data,bool):
    if(bool==1):
        b=data.getPF()
        y=ancienne.getb()
        data.defineresume2(ancienne.getphrase())
        data.definePF(Decimal(b)*Decimal(y))
    ancienne.clear()
    sol_cal=sol(Fenetre,event_menu, endSol,event_nitrate2,data)
    sol_cal.print()


def endSol(ancienne,Fenetre,data,bool):
    if(bool ==1):
        indice=ancienne.getind()[0]
        x= value_file('BDD/sol2.txt')
        data.defineSol(ancienne.getstring(),ancienne.getvalue(),x.getvalueliste(indice))
        data.defineresume3(ancienne.getphrase() + "pour un mh de : "+ str(x.getvalueliste(indice)))
    ancienne.clear()
    prec=precedent(Fenetre,event_menu,endprecedent,endPF,data)
    prec.print()

def endprecedent (ancienne,Fennetre,data,bool):
    if(bool ==1):
        data.defineMr(ancienne.getvalue())
        data.defineresume4(ancienne.getphrase())
        data.defineprecedent(ancienne.getstring())
    else:
        data.resetmpro()
        data.resetresume10()
    ancienne.clear()
    ep= epandages(Fenetre,event_menu,endengre,endSol,data)
    ep.print()

def endengre (ancienne,Fennetre,data,bool):
    if(bool == 1):
        data.definefilename(ancienne.getfilename())
        data.defineresume5(ancienne.getphrase())
    ancienne.clear()
    ep = epandages2(Fenetre,event_menu,endepandage,endprecedent,data)
    ep.print()

def endepandage (ancienne,Fennetre,data,bool):
    if(bool ==1):
        data.defineresume10(ancienne.getphrase())
        data.definempro(ancienne.getvalue())
        x = ancienne.getpred()
    else:
        x=0
    ancienne.clear()
    if(x==1):
        ep= epandages(Fenetre,event_menu,endengre,endprecedent,data)
        ep.print()
    else:
        pra1 = prairie(Fenetre,event_menu,endpra1,endprecedent,data)
        pra1.print()


def endpra1(ancienne,Fenetre,data,bool):
    if (bool ==1):
        data.defineresume7(ancienne.getphrase())
        data.definefilename2(ancienne.getfilename())
    ancienne.clear()
    pra2= prairie2(Fenetre,event_menu,endpra2,endepandage,data)
    pra2.print()

def endpra2(ancienne,Fenetre,data,bool):
    if(bool==1):
        data.defineMhp(ancienne.getvalue())
        data.defineresume8(ancienne.getphrase())
    ancienne.clear()
    nitr = nirr(Fenetre,event_menu,endnirr,endpra1,data)
    nitr.print()

def endnirr(ancienne,Fenetre,data,bool):
    if(bool ==1):
        data.defineNirr(ancienne.getvalue())
        data.defineresume9(ancienne.getphrase())
        data.definevalue(ancienne.getform())
    f=data.getvalue()
    ancienne.clear()
    if (f == 1):
        engr =engraisorg(Fenetre,event_menu,endengr,endpra2,data)
        engr.print()
    else :
        inter = cultinter(Fenetre,event_menu,endcultinter,endpra2,data)
        inter.print()

def endcultinter(ancienne,Fenetre,data,bool):
    if(bool==1):
        data.definefilename3(ancienne.getnamefile())
    else:
        data.resetresume11()
    inter2 = cultinter2(Fenetre,event_menu,endcultinter2,endnirr,data)
    ancienne.clear()
    inter2.print()


def endcultinter2(ancienne,Fenetre,data,bool):
    if(bool ==1):
        data.defineresume11(ancienne.getphrase())
        data.defineMrci(ancienne.getvalue())
    else:
        data.resetresume12()
    ancienne.clear()
    engr = engraisorg(Fenetre,event_menu,endengr,endcultinter,data)
    engr.print()

def endengr(ancienne,Fenetre,data,bool):
    if(bool ==1):
        data.defineresume12(ancienne.getphrase())
        data.defineXpro(ancienne.getvalue())
        data.definevalue2(ancienne.getform())
    s=data.getvalue2()
    ancienne.clear()
    if (s == 1):
        riz = ri(Fenetre,event_menu,endevery,endcultinter2,data)
        riz.print()
    else :
        fin =endscreen(Fenetre,event_menu,endengr,data)
        fin.print()

def endevery(ancienne,Fenetre,data,bool):
    if(bool):
        data.defineresume13(ancienne.getphrase())
        data.defineRi(ancienne.getvalue())
    ancienne.clear()
    fin =endscreen(Fenetre,event_menu,endengr,data)
    fin.print()




#on passe au maïs yes!!






#faire une structure pour chaque partie

def event_maj (menuprincip,Fenetre):
    self.clear()
    maj=maj_file(self.Fenetre)
    maj.print_maj()

def event_menu (truc,Fenetre):
    truc.clear()
    menuprincip=menu(Fenetre,event_nitrate,event_maj)
    menuprincip.print()


Fenetre=Tk()
Fenetre.title('Directives nitrates')
Fenetre.geometry("650x480")
menuprincip=menu(Fenetre,event_nitrate,event_maj)
menuprincip.print()
Fenetre.mainloop()
