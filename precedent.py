from tkinter import *
from decimal import *
from readfile import *
class precedent:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='Quels type de précédentz?')
        self.texte1['fg']='red'
        self.donnees=trt_file('BDD\precedent.txt',Fenetre)
        self.p.add(self.texte1)
        self.p.add(self.donnees.getliste())
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def getphrase(self):
        return "votre précédent est :" +self.donnees.getstringliste()+"il a donc pour valeur mr" +str(self.donnees.getvalueliste())
    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()

    def getind(self):
        return self.donnees.getindliste()
    def getstring(self):
        return self.donnees.getstringliste()
    def getvalue(self):
        return self.donnees.getvalueliste()
