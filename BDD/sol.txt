13
classification de sol :A1  type de sol: Limons sablolimoneux sains
classification de sol :A2  type de sol: Limons argileux profonds et sains
classification de sol :B   type de sol: Limons humides
classification de sol :B1  type de sol: Limons drainés
classification de sol :C1  type de sol: Argiles ou limons argileux profonds
classification de sol :C2  type de sol: Argilo-calcaire profonds
classification de sol :D   type de sol: Argiles humides
classification de sol :E1  type de sol: Graviers profonds
classification de sol :E2  type de sol: Sables profonds
classification de sol :F   type de sol: Gravier superficiels
classification de sol :G   type de sol: Argilo-calcaire superficiels
classification de sol :H1  type de sol: Alluvions organiques sains
classification de sol :H2  type de sol: Marais humides
35
50
35
35
50
50
50
30
35
20
25
50
50
