from tkinter import *
from readfile import *
class dep_esp:

    def __init__(self ,Fenetre,name,fun1,fun2,fun3,data):
        self.Fenetre=Fenetre
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='quels espéces de culture ?')
        self.texte1['fg']='red'
        self.donnees=trt_file(name,Fenetre)
        self.p.add(self.texte1)
        self.p.add(self.donnees.getliste())
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))

    def getind(self):
        return self.donnees.getindliste()
    def getstring(self):
        return self.donnees.getstringliste()
    def getb(self):
        return self.donnees.getvalueliste()
    def getphrase(self):
        return "L'éspéces de votre culture est :" +self.donnees.getstringliste()+"son objectif de rendement(y) est :" + str(self.donnees.getvalueliste())
    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()
