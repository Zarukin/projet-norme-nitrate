from tkinter import *
from decimal import *
from readfile import *
class nirr:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='quantité d\'eau apporté entre le semis et la floraison ? (mm)')
        self.texte1['fg']='red'
        self.spin = Spinbox(Fenetre,from_=0, to=1000000)
        self.texte3=Label(Fenetre,text='Connaissez vous la quantité de nitrate sinon laisser -1 en (mg/L)')
        self.texte3['fg']='red'
        self.spin2=Spinbox(Fenetre,from_=-1, to=1000000)
        self.buttonformulaire = Checkbutton(Fenetre, text="Est -ce une culture d'hiver?",command=self.form)
        self.form=0
        self.p.add(self.texte1)
        self.p.add(self.spin)
        self.p.add(self.texte3)
        self.p.add(self.spin2)
        self.p.add(self.buttonformulaire)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def getphrase(self):
        if(self.spin2.get() == "-1"):
            return "quantité d'eau apporté à la semis et la floraison :"+str(self.spin.get())+" \nvous n'avez pas d'analise de votre eau en nitrate  une valeur de (Nirr):"+str(self.getvalue())
        else:
            return "quantité d'eau apporté à la semis et la floraison :"+str(self.spin.get())+" vous avez  une analise de votre eau en nitrate de valeur :"+str(self.spin2.get()) +" pour une valeur de (Nirr):"+str(self.getvalue())
    def form(self):
        if (self.form == 1):
            self.form =0
        else:
            self.form =1

    def getform(self):
        return self.form

    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()

    def getvalue(self):
        if (self.spin2.get() == "-1"):
                return Decimal(Decimal(Decimal(self.spin.get())/Decimal(100))* Decimal(Decimal(40)/Decimal(4.43)))
        return Decimal(Decimal(Decimal(self.spin.get())/Decimal(100))* Decimal(Decimal(self.spin2.get())/Decimal(4.43)))
