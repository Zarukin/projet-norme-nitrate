from tkinter import *
from readfile import *
class type_culture:

    def __init__(self ,Fenetre,fun1,fun2,fun3):
        self.Fenetre=Fenetre
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='Quels type de culture?')
        self.texte1['fg']='red'
        self.donnees=trt_file("BDD/PF_CEREALES.txt",Fenetre)
        self.texte2=Label(Fenetre,text='Quels département ? (chiffre)')
        self.texte2['fg']='red'
        value = []
        value.append('01')
        value.append('26')
        value.append('38')
        value.append('42')
        value.append('69')
        self.spin = Spinbox(Fenetre, from_=1, to=69,values=value)
        self.buttonformulaire = Checkbutton(Fenetre, text="possédez vous un historique de rendement de l'exploitation pour la culture (5 années)",command=self.form)
        self.form=0
        self.p.add(self.texte1)
        self.p.add(self.donnees.getliste())
        self.p.add(self.texte2)
        self.p.add(self.spin)
        self.p.add(self.buttonformulaire)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,1,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))

    def form(self):
            if (self.form == 1):
                self.form =0
            else:
                self.form =1

    def getform(self):
        return self.form
    def getble(self):
        if (self.donnees.getstringliste() == "Blé dur" or self.donnees.getstringliste() == "Blé tendre"):
            return 1
        return 0
    def getind(self):
        return self.donnees.getindliste()
    def getstring(self):
        return self.donnees.getstringliste()
    def getvalue(self):
        return self.donnees.getvalueliste()
    def getdep(self):
        return self.spin.get()

    def getphrase(self):
        if (self.getble() == 0):
            return "Espéces-Variétés :"+self.donnees.getstringliste() +" par conséquant b =" + str(self.donnees.getvalueliste()) + "\n dans le département :" +str(self.spin.get())
        return "Espéces-Variétés :"+self.donnees.getstringliste() +" par conséquant b ="
    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()
