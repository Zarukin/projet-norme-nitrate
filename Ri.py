from tkinter import *
from decimal import *
class ri:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.p.add(Label(Fenetre,text='Veulliez entrez la mesure de la quantité d\'azote minéral dans le sol à l\'ouverture du bilan'))
        self.spin=Spinbox(Fenetre, from_=0, to=1000)
        self.p.add(self.spin)
        self.data=data
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def getphrase(self):
        return "vous avez une mesure d'azote dan sle sol de "+str(self.getvalue())
    def getvalue(self):
        return Decimal(self.spin.get())
    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()
