from tkinter import *
from decimal import *
from readfile import *
class engraisorg:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='Quels est la teneur en azote de l\'engrais?')
        self.texte1['fg']='red'
        self.spin = Spinbox(Fenetre,from_=0,to=100000)
        self.texte2=Label(Fenetre,text='Quelle est le coéficiant d\'équivalence ?')
        self.texte2['fg']='red'
        self.spin2 = Spinbox(Fenetre,from_=0,to=100000)
        self.texte3=Label(Fenetre,text='Quelle quantité étendue en t/ha')
        self.texte3['fg']='red'
        self.spin3 = Spinbox(Fenetre,from_=0,to=100000)
        self.apprec = Checkbutton(Fenetre, text="Avez vous une analyse de la quantité d'azote dans le sol à l'ouverture du bilan ?",command=self.form)
        self.form=0
        self.p.add(self.texte1)
        self.p.add(self.spin)
        self.p.add(self.texte2)
        self.p.add(self.spin2)
        self.p.add(self.texte3)
        self.p.add(self.spin3)
        self.p.add(self.apprec)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def getphrase(self):
        return "Votre engrais à une teneur en azote de : "+self.spin.get()+" sont coeficiant d'équivalence est : "+self.spin2.get()+"\nvous avez étendue : " +self.spin3.get()+" t/ha pour une valeur total de (Xpro): "+str(self.getvalue())
    def form(self):
        if (self.form == 1):
            self.form =0
        else:
            self.form =1
    def getform(self):
        return self.form

    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()


    def getvalue(self):
        return Decimal(Decimal(Decimal(self.spin.get())*Decimal(self.spin2.get()))*Decimal(self.spin3.get()))
