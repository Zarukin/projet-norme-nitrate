from tkinter import *
from decimal import *
from readfile import *
class prairie2:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.f = Fenetre
        self.texte1=Label(Fenetre,text='qulle age à la prairie')
        self.texte1['fg']='red'
        self.list = trt_file(data.getfilename2(),Fenetre)
        self.texte3=Label(Fenetre,text='Quelle est le mode d\'exploitation ?')
        self.texte3['fg']='red'
        value = []
        value.append('Pature integrale')
        value.append('fauche + pature')
        value.append('fauche intégrale')
        self.spin = Spinbox(Fenetre,values=value)
        self.p.add(self.texte1)
        self.p.add(self.list.getliste())
        self.p.add(self.texte3)
        self.p.add(self.spin)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def getphrase(self):
        return "la prairie à "+self.list.getstringliste()+"le mode d'exploitation est "+self.spin.get()+" ce qui fait un Mhp de"+str(self.getvalue())
    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()

    def getvalue(self):
        print(self.list.getvalueliste())
        if ( self.spin.get() == 'Pature integrale'):
            return self.list.getvalueliste()
        if (self.spin.get() == 'fauche + pature'):
            return Decimal(Decimal(self.list.getvalueliste()) * Decimal(0.7))
        return Decimal(Decimal(self.list.getvalueliste()) * Decimal(0.4))
