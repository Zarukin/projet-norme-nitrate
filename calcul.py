from tkinter import *
from decimal import *

class calcul :
    def __init__(self):
        self.PF=Decimal(0)
        self.TypeProduction=''
        self.Dep=''
        self.form=Decimal(0)
        self.sol=''
        self.solMh=Decimal(0)
        self.solRf=Decimal(0)
        self.Mr=Decimal(0)
        self.Mpro=Decimal(0)
        self.Mhp=Decimal(0)
        self.Nirr=Decimal(0)
        self.Mrci=Decimal(0)
        self.Xpro =Decimal(0)
        self.Prec=''
        self.cont=Decimal(0)
        self.Ri=Decimal(-1)
        self.P=Decimal(0)
        self.resume1=''
        self.resume2=''
        self.resume3=''
        self.resume4=''
        self.resume5=''
        self.resume6=''
        self.resume7=''
        self.resume8=''
        self.resume9=''
        self.resume10=''
        self.resume11=''
        self.resume12=''
        self.resume13=''
        self.filename=''
        self.filename2=''
        self.filename3=''
        self.value=0
        self.value2=0
    def defineresume13(self,x):
        self.resume13=self.resume13 +x
    def getresume13(self):
        return self.resume13
    def resetresume13(self):
        self.resume13=''
    def defineresume12(self,x):
        self.resume12=self.resume12 +x
    def getresume12(self):
        return self.resume12
    def resetresume12(self):
        self.resume12=''
    def defineresume11(self,x):
        self.resume11=self.resume11 +x
    def getresume11(self):
        return self.resume11
    def resetresume11(self):
        self.resume11=''
    def defineresume10(self,x):
        self.resume10=self.resume10 +x
    def resetresume10(self):
        self.resume10=''
    def getresume10(self):
        return self.resume10
    def defineresume9(self,x):
        self.resume9=x
    def getresume9(self):
        return self.resume9
    def defineresume8(self,x):
        self.resume8=x
    def getresume8(self):
        return self.resume8
    def defineresume7(self,x):
        self.resume7=x
    def getresume7(self):
        return self.resume7
    def defineresume6(self,x):
        self.resume6=x
    def getresume6(self):
        return self.resume6
    def defineresume5(self,x):
        self.resume5=x
    def getresume5(self):
        return self.resume5
    def defineresume4(self,x):
        self.resume4=x
    def getresume4(self):
        return self.resume4
    def defineresume3(self,x):
        self.resume3=x
    def getresume3(self):
        return self.resume3
    def defineresume2(self,x):
        self.resume2=x
    def getresume2(self):
        return self.resume2
    def defineresume1(self,x):
        self.resume1=self.resume1 +x
    def resetresume1(self):
        self.resume1=''
    def getresume1(self):
        return self.resume1

    def getfilename(self):
        return self.filename
    def definefilename(self,s):
        self.filename=s
    def getvalue(self):
        return self.value
    def definevalue(self,s):
        self.value=s
    def getvalue2(self):
        return self.value2
    def definevalue2(self,s):
        self.value2=s
    def getfilename3(self):
        return self.filename3
    def definefilename3(self,s):
        self.filename3=s
    def getfilename2(self):
        return self.filename2
    def definefilename2(self,s):
        self.filename2=s
    def definePF(self,x):
        self.PF=x
    def defineTypeProduction(self,s):
        self.TypeProduction=s
    def defineSol(self,s,x,y):
        self.sol=s
        self.solMh=y
        self.solRf=x
    def defineDep (self,i):
        self.Dep=i
    def defineform (self,x):
        self.form=x
    def defineMr(self,x):
        self.Mr=x
    def defineMhp(self,x):
        self.Mhp=x
    def defineNirr(self,x):
        self.Nirr=x
    def defineMrci(self,x):
        self.Mrci=x
    def defineXpro(self,x):
        self.Xpro = x
    def getNirr(self):
        return self.Nirr
    def getMhp(self):
         return self.Mhp
    def getMr(self):
        return self.Mr
    def definempro(self,x):
        self.Mpro=self.Mpro*self.cont
        self.cont = self.cont +1
        self.Mpro=Decimal(Decimal(self.Mpro+x)/Decimal(self.cont))
    def resetmpro(self):
        self.Mpro=Decimal(0)
        self.cont=Decimal(0)
    def defineprecedent(self,x):
        self.Prec=x
    def getsol(self):
        return self.sol
    def getsolMh(self):
        return self.solMh
    def getsolRf(self):
        return self.solrf
    def getPF(self):
        return self.PF
    def getTypeProduction(self):
        return self.TypeProduction
    def getDep(self):
        return self.Dep
    def getform(self):
        return self.form
    def getMpro(self):
        return self.Mrpo
    def defineRi(self,x):
        self.Ri=Decimal(x)
    def calculRI(self):
        if(self.Ri==-1):
            s=0
            p=0
            if ((self.sol=="classification de sol :E1  type de sol: Graviers profonds" ) or (self.sol=="classification de sol :F   type de sol: Gravier superficiels" ) or (self.sol =='classification de sol :G   type de sol: Argilo-calcaire superficiels' )):
                s=1
            if (self.Prec == 'tournesol' or self.Prec == 'sorgho'or self.Prec == 'jachère de graminées'or self.Prec == 'tabac blond'):
                p=1
            elif(self.Prec == 'Céréales à paille enlevée'or self.Prec == 'Céréales à pailles enfouies'or self.Prec == 'maïs grain'or self.Prec == 'colza'or self.Prec == 'autres cultures'):
                p=2
            if (s == 0):
                if(p==0):
                    self.Ri=Decimal(55)
                elif(p == 2):
                    self.Ri=Decimal(45)
                else:
                    self.Ri=Decimal(40)
            else:
                if(p==0):
                    self.Ri=Decimal(25)
                elif(p == 2):
                    self.Ri=Decimal(20)
                else:
                    self.Ri=Decimal(15)
            self.defineresume13("Votre RI calculé est :" + str(self.Ri))
    def calculP(self):
        self.P=self.solMh+self.Mr+self.Mpro
    def calculerC (self):
        self.calculRI()
        self.calculP()
        return Decimal(self.PF +self.solRf-self.P-self.Mhp-self.Mrci-self.Nirr-self.Xpro-self.Ri)
