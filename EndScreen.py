from tkinter import *
from decimal import *
from readfile import *
class endscreen:
    def __init__ (self,Fenetre,fun1,fun2,data):
        self.texte1=Label(Fenetre,text='Votre bilan est :')
        self.texte1['fg']='red'
        self.texte2=Label(Fenetre,text=str(data.calculerC()))
        self.texte3=Label(Fenetre,text=str(data.getresume1()))
        self.texte5=Label(Fenetre,text=str(data.getresume2()))
        self.texte6=Label(Fenetre,text=str(data.getresume3()))
        self.texte7=Label(Fenetre,text=str(data.getresume4()))
        self.texte8=Label(Fenetre,text=str(data.getresume5()))
        self.texte9=Label(Fenetre,text=str(data.getresume10()))
        self.texte11=Label(Fenetre,text=str(data.getresume7()))
        self.texte12=Label(Fenetre,text=str(data.getresume8()))
        self.texte13=Label(Fenetre,text=str(data.getresume9()))
        self.texte14=Label(Fenetre,text=str(data.getresume11()))
        self.texte15=Label(Fenetre,text=str(data.getresume12()))
        self.texte16=Label(Fenetre,text=str(data.getresume13()))
        self.buttonMenu=Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre))


    def print(self):
        self.texte1.pack()
        self.texte2.pack()
        self.texte3.pack()
        self.texte5.pack()
        self.texte6.pack()
        self.texte7.pack()
        self.texte8.pack()
        self.texte9.pack()
        self.texte11.pack()
        self.texte12.pack()
        self.texte13.pack()
        self.texte14.pack()
        self.texte15.pack()
        self.texte16.pack()
        self.buttonMenu.pack()

    def clear(self):
        self.texte1.destroy()
        self.texte2.destroy()
        self.texte3.destroy()
        self.texte5.destroy()
        self.texte6.destroy()
        self.texte7.destroy()
        self.texte8.destroy()
        self.texte9.destroy()
        self.texte11.destroy()
        self.texte12.destroy()
        self.texte13.destroy()
        self.texte14.destroy()
        self.texte15.destroy()
        self.texte16.destroy()
        self.buttonMenu.destroy()
