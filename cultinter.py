from tkinter import *
from decimal import *
from readfile import *
class cultinter:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='à quels période à lieu l\'ouverture du bilan ?')
        self.texte1['fg']='red'
        value = []
        value.append('ouverture du bilan en sortie d\'hiver')
        value.append('ouverture du bilan en Avril')
        self.spin = Spinbox(Fenetre,values=value)
        self.texte2=Label(Fenetre,text='Quand la destruction à t-elle lieu ?')
        self.texte2['fg']='red'
        value2 = []
        value2.append('Destruction Nov /dec')
        value2.append('Destruction > Janv')
        self.spin2 = Spinbox(Fenetre,values=value2)
        self.texte3=Label(Fenetre,text='Production de Culture (tMS/ha) ?')
        self.texte3['fg']='red'
        value3 = []
        value3.append('<=1')
        value3.append('>1 et <3')
        value3.append('>=3')
        self.spin3 = Spinbox(Fenetre,values=value3)
        self.p.add(self.texte1)
        self.p.add(self.spin)
        self.p.add(self.texte2)
        self.p.add(self.spin2)
        self.p.add(self.texte3)
        self.p.add(self.spin3)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))


    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()
    def getphrase(self):
        return self.spin.curselection()+"la destruction  à eu lieu "+self.spin2.curselection()+"la production de culture:"+self.spin3.curselection()+" tMs/ha"
    def getnamefile(self):
        x='0'
        y='0'
        z='0'
        if(self.spin.get() == 'ouverture du bilan en sortie d\'hiver'):
            x='1'
        else :
            x='2'
        if(self.spin2.get() =='Destruction Nov /dec' ):
            y='1'
        else:
            y='2'
        if(self.spin3.get() == '<=1'):
            z='1'
        elif(self.spin3.get() == '>1 et <3' ):
            z='2'
        else:
            z='3'
        return 'BDD/cultinter' + x + y + z +'.txt'
