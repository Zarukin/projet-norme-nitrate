from tkinter import *
from decimal import *
from readfile import *
class epandages:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.texte1=Label(Fenetre,text='Quelle est la fréquence d\'épandage sur votre champs ?')
        self.texte1['fg']='red'
        self.freq = Listbox(Fenetre)
        self.freq.insert(1, "Tous les 2 ans")
        self.freq.insert(2, "Tous les 3 ans")
        self.freq.insert(3, "apport occasionel maximum tous les 4 ans")
        self.apprec = Checkbutton(Fenetre, text="apport sur le précédent",command=self.form)
        self.form=0
        self.p.add(self.texte1)
        self.p.add(self.freq)
        self.p.add(self.apprec)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))
    def form(self):
        if (self.form == 1):
            self.form =0
        else:
            self.form =1

    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()

    def getphrase(self):
        if (self.form == 1):
            return "Vous épandez " + str(self.freq.curselection()) + " il y a eu un apport sur le précédent"
        else:
            return "Vous épandez " + str(self.freq.curselection()) + " il n'y a  pas eu d'apport sur le précédent"

    def getfilename(self):
        return 'BDD/epandages'+ str(self.form) +str(self.freq.curselection()[0]) +'.txt'
