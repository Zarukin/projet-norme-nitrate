from tkinter import *
from decimal import *
from readfile import *
class prairie:
    def __init__ (self,Fenetre,fun1,fun2,fun3,data):
        self.p = PanedWindow(Fenetre, orient=VERTICAL,width=470,height=350)
        self.f = Fenetre
        self.texte1=Label(Fenetre,text='à quels moment la destruction à lieu ')
        self.texte1['fg']='red'
        self.freq = Listbox(Fenetre)
        self.freq.insert(1, "printemps")
        self.freq.insert(2, "autonme")
        self.texte4=Label(Fenetre,text='combien de culture incluant celle si as t\'il eu après le retournement  ?')
        self.texte4['fg']='red'
        self.spin = Spinbox(Fenetre, from_=1, to=2)
        self.p.add(self.texte1)
        self.p.add(self.freq)
        self.p.add(self.texte4)
        self.p.add(self.spin)
        self.p2=PanedWindow(Fenetre, orient=HORIZONTAL,width=470,height=10)
        self.p2.add(Button(Fenetre,text='Valider',command=lambda: fun2(self,Fenetre,data,1),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='Precedent',command=lambda: fun3(self,Fenetre,data,0),height = 10, width = 30))
        self.p2.add(Button(Fenetre,text='retour menu',command=lambda: fun1(self,Fenetre),height = 10, width = 30))
        self.p.add(Label(Fenetre,text=''))

    def print(self):
        self.p.pack(fill = BOTH, expand = 1)
        self.p2.pack(fill = BOTH, expand = 1)
    def clear(self):
        self.p.destroy()
        self.p2.destroy()

    def getphrase(self):
        return "la destruction à lieu en "+str(self.freq.curselection())+"il y'a eu "+str(self.spin.get())+"culture incluant celle-ci après le retournement"
    def getind(self):
        return self.donnees.getindliste()
    def getstring(self):
        return self.donnees.getstringliste()
    def getfilename(self):
        if(self.freq.curselection()[0] == 0):
            print("printemp")
            return "BDD\prairieP" + self.spin.get() +".txt"
        print ("autone")
        return "BDD\prairieA.txt"
